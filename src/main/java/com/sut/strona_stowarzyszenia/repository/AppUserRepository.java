package com.sut.strona_stowarzyszenia.repository;


import com.sut.strona_stowarzyszenia.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface AppUserRepository extends JpaRepository<AppUser,Long> {
     Optional<AppUser> findByEmail(String email);

     boolean existsByEmail(String email);
     void deleteAppUserByIdAfter(Long idAfter);
 }
