package com.sut.strona_stowarzyszenia.service;



import com.sut.strona_stowarzyszenia.model.AppUser;

import java.util.List;

public interface UserService {

    void registerUser(String username, String password, String passwordConfirm);

    List<AppUser> getAllUsers();
}
