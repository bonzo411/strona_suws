package com.sut.strona_stowarzyszenia.service;


import com.sut.strona_stowarzyszenia.model.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.List;

public interface MemberService {

    List<Member> getAllMembers();
    Page<Member> getAllMembersPageable(String pageNo);
    List<Integer> getPageNumberList();

    void addMembersListFromExcel(String sciezka) throws IOException;

    void addMember(Member member);

    void removeMemberByAlbumNumber(String albumNumber);

    void update(String albumNumber, Member memberUpdate);


}
