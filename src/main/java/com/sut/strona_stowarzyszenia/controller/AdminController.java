package com.sut.strona_stowarzyszenia.controller;

import com.sut.strona_stowarzyszenia.model.Member;
import com.sut.strona_stowarzyszenia.service.MemberService;
import com.sut.strona_stowarzyszenia.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequestMapping("/admin")
public class AdminController {
  @Autowired
    private UserService userService;
    @Autowired
    private MemberService memberService;

    @GetMapping("")
    private String adminStartWindow(){ return "admin_page/adminStartWindow";}


    @GetMapping("/user/list")
    public String getUserList(Model model){
        model.addAttribute("userList",userService.getAllUsers());
          return "admin_page/userList";
    }

    @GetMapping("/member/add/excel/list")
    public String addMembersOutExcelForm(){ return "admin_page/addMembersOutExcel";}

    @PostMapping("/member/add/excel/list")
    public String submitAddMembersOutExcelForm(@RequestParam(name="sciezka") String sciezka) throws IOException {
        memberService.addMembersListFromExcel(sciezka);

        return "redirect:/members/list/search";
    }

    @GetMapping("/member/add/one")
    public String addMemberForm(Model model){
        model.addAttribute("Member",new Member());
        return "admin_page/addMember";
    }

    @PostMapping("/member/add/one")
    public String addMemberSubmit(Member member){
        memberService.addMember(member);
        return "redirect:/admin/member/add/one";
    }

    @GetMapping(path = "/member/delete/one")
    public String deleteMemberForm(){
        return "admin_page/deleteMember";
    }

    @PostMapping(path = "/member/delete/one")
    public String deleteMember(@RequestParam(name ="albumNumber") String albumNumber){
        memberService.removeMemberByAlbumNumber(albumNumber);
        return  "redirect:/admin/member/delete/one";
    }

    @GetMapping("/member/update")
    public String update(Model model){
        model.addAttribute("memberUpdate",new Member());
        return "admin_page/updateMember";
    }

    @PostMapping("/member/update")
    public String update(@RequestParam(name = "albumNumber")String albumNumber,
                         Member memberUpdate){
        memberService.update(albumNumber, memberUpdate);
        return "redirect:/admin";
    }

}
