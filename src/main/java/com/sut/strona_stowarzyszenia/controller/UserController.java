package com.sut.strona_stowarzyszenia.controller;


import com.sut.strona_stowarzyszenia.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/user")
public class UserController {
//todo: zamienić autowiredy na konstruktory zamiast pól
    @Autowired
    private UserService userService;


    @GetMapping("/register")
    public String getRegisterForm(){
        return "userRegister";
    }

    @PostMapping("/register")
    public String submitRegisterForm(@RequestParam(name="username") String username,
                                     @RequestParam(name="password")String password,
                                     @RequestParam(name="password-confirm") String passwordConfirm){
       userService.registerUser(username,password,passwordConfirm);

        return "redirect:/login";
    }

}
