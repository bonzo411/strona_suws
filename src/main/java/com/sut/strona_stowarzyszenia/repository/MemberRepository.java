package com.sut.strona_stowarzyszenia.repository;


import com.sut.strona_stowarzyszenia.model.Member;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface MemberRepository extends JpaRepository<Member , Long> {


    void deleteMemberByAlbumNumber(String albumNumber);

    Optional<Member> findMemberByAlbumNumber(String albumNumber);

    List<Member> findByLastNameLike(String lastName, Pageable pageable);



}
