package com.sut.strona_stowarzyszenia.service;

import com.sut.strona_stowarzyszenia.model.UserRole;
import com.sut.strona_stowarzyszenia.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    //value laduje wartosci z ustawien(properties) do zmiennych
    @Value("${user.default.roles}")
    private String[] defaultRoles;


    @Override
    public Set<UserRole> getDefaultUserRoles() {
        HashSet<UserRole> roles = new HashSet<>();

        for(String role : defaultRoles){
            roles.add(userRoleRepository.findByName(role));
        }
        return roles;
    }


}
