package com.sut.strona_stowarzyszenia.repository;


import com.sut.strona_stowarzyszenia.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<UserRole,Long> {

    boolean existsByName(String name);

    UserRole findByName(String role);

}
