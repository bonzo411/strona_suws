package com.sut.strona_stowarzyszenia.service;


import com.sut.strona_stowarzyszenia.model.UserRole;

import java.util.Set;

public interface UserRoleService {
   Set<UserRole> getDefaultUserRoles();
}
