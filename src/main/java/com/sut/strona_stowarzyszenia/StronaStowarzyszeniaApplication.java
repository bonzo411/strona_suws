package com.sut.strona_stowarzyszenia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StronaStowarzyszeniaApplication {

    public static void main(String[] args) {
        SpringApplication.run(StronaStowarzyszeniaApplication.class, args);
    }

}
